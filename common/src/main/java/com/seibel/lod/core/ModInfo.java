/*
 *    This file is part of the Distant Horizon mod (formerly the LOD Mod),
 *    licensed under the GNU GPL v3 License.
 *
 *    Copyright (C) 2020  James Seibel
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, version 3.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.seibel.lod.core;

/**
 * This file is similar to mcmod.info
 * <br>
 * If you are looking at this mod's source code and don't
 * know where to start.
 * Go to the api/lod package (folder) and take a look at the ClientApi.java file,
 * Pretty much all of the mod stems from there.
 * 
 * @author James Seibel
 * @version 11-13-2021
 */
public final class ModInfo
{
	public static final String ID = "lod";
	/** The internal mod name */
	public static final String NAME = "DistantHorizons";
	/** Human readable version of NAME */
	public static final String READABLE_NAME = "Distant Horizons";
	public static final String API = "LodAPI";
	public static final String VERSION = "a1.5.3";
}